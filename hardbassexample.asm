;	HARDBASS+RMT Playback Example
;	Created by Sandor Teli / HARD (10/2016-11/2017)
;
;	requirements: 64K PAL XL/XE
;
;	Note: This is a simple example demonstrating how to
;	play a HARDBASS+RMT tune using screen kernel + IRQ based updates.
;	For other examples like updating HARDBASS from purely IRQs + DLIs
;	or playing a HARDBASS+RMT tune alongside a HCM picture
;	see the REHARDEN demo and its source code repo.
;
;	The screen kernel + IRQ based approach used in this example
;	(and also in the HCM screens in REHARDEN) is generally easier to
;	implement than the IRQ+DLI based solution used in the Twister-Scroller
;	screen in REHARDEN.
;	The reason is that DLIs/IRQs affect each other and are affected by DMAs
;	therefore getting the right timing for your unique screen setup can be
;	quite a hit and miss situation unless you're very experienced with these
;	mechanisms and their interaction.
;	The screen kernel + IRQ approach is not affected by this challenge as
;	it's not using DLIs and controls the realignment of the 64KHz timer in
;	the main loop.
;
;	On a side note:
;	It'd be nice to create some sort of a reusable library out of this code
;	rather than having people to copy-paste-modify.

	ICL "equates.asm"

	rmt_music_data = $d800

	rmt_player = PLAYER

	.def visualization
;	.def testcolors
;	.def debugtracker
;	.def debugtimers
	.def rmt

	;code not tested for other than #4
	timer = 4

	.ifndef debugtimers
	sample_register = AUDC1+(timer-1)*2
	.else
	sample_register = COLBK
	.endif

	;HARDBASS settings I used in REHARDEN
	;didn't test/validate any other settings seriously
	num_samples = 312>>3 ;# of scanlines/8
	cutoff_range = $f0
        cutoff_margin = (255-cutoff_range) >> 1
        cutoff_step = cutoff_range >> 4
        max_pulse_inc = 6

	;the REHARDEN RTM tune is under the ROM
	;so we'll make a copy of the Atari font
	font = $1c00

	org $2000

init	sei

	lda #$00
	sta NMIEN
	sta IRQEN

	;let's make a copy of the Atari font
	ldx #$00
cpf	lda $e000,x
	sta font,x
	lda $e100,x
	sta font+$100,x
	lda $e200,x
	sta font+$200,x
	lda $e300,x
	sta font+$300,x
	inx
	bne cpf

	jsr waitforvblank

	lda #$b2
	sta PORTB

	lda #>font
	sta CHBASE

	lda #<dlist
	sta DLISTL
	lda #>dlist
	sta DLISTH
	
	lda #$00
	sta COLBK
	sta COLPF2
	lda #$0e
	sta COLPF1

	rts

waitforvblank
	lda #$7c
waitforvcount
	cmp VCOUNT
	bne waitforvcount
	rts

	;simple text DLIST
dlist
	.byte $70,$70,$70

	.byte $42,<screen,>screen
	.rept 23, #
	.byte $02
	.endr

	.byte $41, <dlist, >dlist

	;screen contents
screen
	.byte "                                "
	.byte " ------------------------------ "
	.byte "                                "
	.byte " HARDBASS+RMT Playback Example  "
	.byte "                                "
	.byte " ------------------------------ "
	.byte "                                "
	.byte " Demonstrating:                 "
	.byte "                                "
	.byte " - Screen kernel + IRQ driven   "
	.byte "   bass updates                 "
	.byte " - HARDBASS+RMT sharing Chl #4  "
	.byte " - Small modifications to       "
	.byte "   rmtplayr.asm for chl sharing "
	.byte "                                "
	.byte " For more advanced examples see "
	.byte " the REHARDEN source code doing "
	.byte " screen kernel based updates    "
	.byte " while displaying HCM pictures  "
	.byte " and doing IRQ + DLI based bass "
	.byte " updates in the Greetings part. "
	.byte "                                "
	.byte " 11/2017            Sandor/HARD "
	.byte "                                "

	ini init


	;sample buffers (there's two)
	org $40e8
samplebuf
	;this must remain aligned to xxe8 so that the IRQ can read from the start of a page (+$18)

	org samplebuf+$100
samplebuf2
	;this must remain aligned to xxe8 so that the IRQ can read from the start of a page (+$18)
	.ds num_samples
	
	;filter related lookup tables for HARDBASS
filter_tbls
	.byte $f0, $e0, $d0, $c0, $b0, $a0, $90, $80, $70, $60, $50, $40, $30, $20, $10, $00

filter_dshim1s
	.ds 16

	.align $100
cutoff_scale_old .ds $100

	.align $100
	;aligned to page start so that the IRQ code can be a tad faster
timerpattern
	;with these timer values I attempt to give us a stable average
	;so that our timer needs very little re-alignment each frame
	;(each 7th IRQ should be aligned to "char" start)
	.byte $1f, $20, $20, $1f, $20, $1f, $20
	
	timerpatternmax = 6

start	sei

	lda #$00
	sta NMIEN
	sta IRQEN

	lda #<zpcode.irq
	sta $fffe
	lda #>zpcode.irq
	sta $ffff

	ldx #<rmt_music_data	;low byte of RMT module to X reg
	ldy #>rmt_music_data	;hi byte of RMT module to Y reg
	lda #0			;starting song line 0-255 to A reg
	jsr rmt_player		;Init
	
	lda #0 ;stick with 64k timers
	sta AUDCTL			
	lda #3
	sta SKCTL
	
	jsr copyzp
	jsr initsound
	jsr inittracker
	
	.ifndef debugtimers
	;HARDBASS works with two small buffers
	;pre-buffer one frame worth of sound so that we're in sync with RMT
	;we use buff2 first as that's how the rest of the code starts out
	jsr dotracker
	jsr setsoundbuff2
	jsr dosound
	.endif
	
	jsr waitforvblank

	lda #$21
	sta DMACTL

	cli

loop	

	;swap sample buffers in the screen kernel code
	lda sb1+2
	eor #((samplebuf>>8)^(samplebuf2>>8))
	sta sb1+2
	lda #<samplebuf
	sta sb1+1

	;we wait until the kernel is just about to start
	lda #$0f
	jsr waitforvcount

	ldx #$00

	;we don't want IRQs during our kernel
	stx IRQEN

	sta WSYNC
	
stxaudf
	;timer freq = 0; part of our timer realignment process
	;this makes sure it runs out as fast as possible
	;we'll wait 192 scan lines, but I like having this anyways...
	stx AUDF1+(timer-1)*2
	
	;resettig the timer pattern in the IRQ code
	stx zpcode.irqsplcnt+1

lineloop
	;Let's do the kernel work in sets of 8 scanlines
	;this lets us perform sub-tasks that correspond to the same higher level task
	;in each member of the group while the kernel still remains small.
	;You can choose to do your kernel in a very different way of course.
	.rept 8, #

	.if :1==0
	;line #0 of the group ("char") will read the sample data here
sb1	ldy samplebuf
	.else
	;all other lines can spend 4 cycles here doing something else
	nop
	nop
	.endif

	sta WSYNC

	.if :1==0
styaudc
	;line #0 of the group will also write the sample data
	sty sample_register
	nop ;padding to 6 cycles so your kernel remains tidy
	.elseif :1==1
	;line #1 of the group increments the sample buffer address in line #0
	inc sb1+1
	.else
	;all other lines can spend 6 cycles here as they don't contribute to updating HARDBASS
	nop
	nop
	nop
	.endif
	
	;you have free cycles to use here in all lines of the group
	;in REHARDEN I'm using this time to replicate players for the HCM display

	.ifndef debugtimers
	.ifndef testcolors
	.ifdef visualization
	;dummy work done in kernel - creating a striped playfield and displaying sample data
	;for a more advanced example see the HCM kernel in REHARDEN that drives HARDBASS
	sty COLBK
	txa
	asl
	and #$02
	sta COLPF2
	.endif
	.endif
	.endif

	inx
	.endr

	cpx #$c0
	beq afterscreen

	jmp lineloop
	
afterscreen

	;let's swap sample buffers in the IRQ code before we let IRQs run again
	jsr swapirqsamplebuffers

	;now that the last timer has surely run out (we set it freq to zero earlier and waited a lot since)
	;let's re-initialize the timer frequency with the 1st element of our timer pattern
	jsr restarttimer
	
	;see if RMT wants HARDBASS to play
	lda timerena
	beq noirq

	;let's kick off our IRQ chain manually
	;the 1st IRQ will get the subsequent ones going
	brk
	nop ;brk / rti padding

afterbrk
	jsr handlesound
	jmp loop

swapirqsamplebuffers
	lda zpcode.sb2+2
	eor #(((samplebuf>>8)+1)^((samplebuf2>>8)+1))
	sta zpcode.sb2+2
	lda #$00
	sta zpcode.sb2+1
	rts

noirq
	;RMT owns channel #4, let's make sure IRQs don't run
	sta IRQEN
	jmp afterbrk

	jmp loop
	
	;this takes care of sound generation
	;for both RMT and HARDBASS
handlesound
	.ifndef debugtimers
	.ifdef testcolors
	lda #$86
	sta COLBK
	.endif
	.endif

	.ifndef debugtimers
	;Calling the HARDBASS tracker playback code
	jsr dotracker
	.endif
	
	.ifndef debugtimers
	.ifdef testcolors
	lda #$26
	sta COLBK
	.endif
	.endif

	.ifndef debugtimers
	;Generating a frame worth of HARDBASS sound into the active buffer
	;based on the settings the tracker playback code just set
	jsr dosound
	.endif
	
	.ifndef debugtimers
	.ifdef testcolors
	lda #$fc
	sta COLBK
	.endif
	.endif
		
	.ifdef rmt
	;Calling the RMT player
	jsr rmt_player+3		;play
	.endif

	.ifndef debugtimers
	.ifdef testcolors
	lda #$00
	sta COLBK
	.endif
	.endif
	rts
	
	;re-initializes the timer frequency
restarttimer
	lda #timerpatternmax-1
	sta zpcode.irqtpt+1

	lda timerpattern+timerpatternmax
	sta WSYNC

staaudf	
	sta AUDF1+(timer-1)*2

	rts

	;initializes the HARDBASS sound buffers
	;with either empty contents or a test pattern
initsound
	jsr initfilter

	.ifndef debugtimers
	ldx #$00
	lda #$10
imt0	sta samplebuf, x
	sta samplebuf2, x
	inx
	cpx #num_samples
	bne imt0
	rts
	.else
	ldx #$00
	lda #$10
	
imt1	eor #$0f
	sta samplebuf, x
	sta samplebuf2, x
	inx
	cpx #num_samples
	bne imt1
	rts	
	.endif
	
	;HARDBASS sound engine
dosound
	ldx #$100-num_samples
dshist	ldy #0 ;reading history from last round
		
dsnext

dsaccl	lda #$00 ;accu lo
	clc ;todo: review/remove - might not need this
dsincl	adc #$00 ;increment lo
	sta dsaccl+1
	
dsacch	lda #$00 ;accu hi
dsinch	adc #$00 ;increment hi
	sta dsacch+1
		
dspw	cmp #$00 ;pulse width
		
dsflttbl
	lda cutoff_scale_old+$00,y ;scale history
	bcc dsstor ;see prev cmp
		
dshim1	adc #$00 ;C is always set here! make sure to store value-1
		
dsstor	tay ;put result into history
	ora #$10
dsbuff	sta samplebuf-($100-num_samples),x

	inx
	bne dsnext
	
	sty dshist+1 ;persist history for next round

	;swap buffers
	lda dsbuff+2
	cmp #>(samplebuf-($100-num_samples))
	beq setsoundbuff2
	
	;sets sound buffer #1 for the sound generator
setsoundbuff1
	lda #<(samplebuf-($100-num_samples))
	sta dsbuff+1
	lda #>(samplebuf-($100-num_samples))
	sta dsbuff+2
	rts
	
	;sets sound buffer #2 for the sound generator
setsoundbuff2
	lda #<(samplebuf2-($100-num_samples))
	sta dsbuff+1
	lda #>(samplebuf2-($100-num_samples))
	sta dsbuff+2
	rts

	;initializes the HARDBASS filter tables
initfilter
	lda #<cutoff_scale_old
	sta cutofftbl+1
	lda #>cutoff_scale_old
	sta cutofftbl+2
	
	ldx #$00
	lda #cutoff_margin
if0	sta cutofftarget+1
	pha
	txa
	pha

	jsr initcutoff
	
	pla
	tax
	pla
	clc
	adc #cutoff_step
	inx
	cpx #$10
	bne if0
	rts
	
initcutoff
	ldx #$00
	ldy #$00
	lda #$00
	clc
	
cutofftbl
	sty cutoff_scale_old

cutofftarget
	adc #$05 ;target value
	bcc icnext
	iny
	clc
	
icnext
	inc cutofftbl+1
	inx
	cpx #$10
	bne cutofftbl
	rts

	;initializes the HARDBASS tracker playback engine	
inittracker
	lda #$00
	sta instrFlags
	sta baseinclo+1
	sta baseinchi+1
	sta basecutoff+1
	sta basepw+1
	sta pwlfo_value+1
	sta pwlfo_inc+1
	sta pwlfo_depth+1
	sta dotracker_pwlfo_reverse_on_off+1
	sta cutofflfo_value+1
	sta cutofflfo_inc+1
	sta cutofflfo_depth+1
	sta dotracker_cutofflfo_reverse_on_off+1
	sta vib_value_lo+1
	sta vib_value_hi+1
	sta vib_inc_lo+1
	sta vib_inc_hi+1
	sta vib_depth+1
	sta dotracker_vib_lo_reverse_on_off+1
	sta dotracker_vib_hi_reverse_on_off+1
	
	.ifndef debugtimers
	.ifdef debugtracker
	lda #$ff
	sta trackerframe
	.endif
	.endif

	lda #$01
	sta pattern_skip_frames
		
	lda #$80
	sta dotracker_porta_on_off+1

	//TODO: Support no-loop ($ff)
	lda #<hbassdata
	clc
	adc song_loop_pos_addr
	sta loop_addr_lo+1
	lda #>hbassdata
	adc #$00
	sta loop_addr_hi+1
	
	lda #<note_tbl_lo_addr
	clc
	adc note_count_addr
	sta note_tbl_hi_addr+1
	lda #>note_tbl_lo_addr
	adc #$00
	sta note_tbl_hi_addr+2

	lda note_count_addr
	asl
	clc
	adc #<note_tbl_lo_addr
	sta pattern_index_cursor
	sta zp_tmp
	sta max_pattern_addr_addr+1
	lda #>note_tbl_lo_addr
	adc #$00
	sta pattern_index_cursor+1
	sta zp_tmp+1
	sta max_pattern_addr_addr+2
	
	ldx #$00
max_pattern_addr_addr
	lda $1111,x
	sta max_pattern_addr,x
	inx
	cpx #$02
	bne max_pattern_addr_addr
	
	lda max_pattern_addr
	clc
	adc #<hbassdata
	sta max_pattern_addr
	lda max_pattern_addr+1
	adc #>hbassdata
	sta max_pattern_addr+1

pattern_addr_reloc
	ldy #$00
	lda (zp_tmp),y
	clc
	adc #<hbassdata
	sta (zp_tmp),y
	iny
	lda (zp_tmp),y
	adc #>hbassdata
	sta (zp_tmp),y
	lda zp_tmp
	clc
	adc #$02
	sta zp_tmp
	bcc pattern_addr_reloc0
	inc zp_tmp+1
pattern_addr_reloc0
	lda zp_tmp
	cmp max_pattern_addr
	bne pattern_addr_reloc
	lda zp_tmp+1
	cmp max_pattern_addr+1
	bne pattern_addr_reloc
	
	jsr loadpattern
	
	ldx #$00
	ldy #$ff
filter_tbl_helper_loop	
	lda cutoff_scale_old,y
	eor #$0f
	sec
	sbc #$01
	cmp #max_pulse_inc
	bcc finrange
	lda #max_pulse_inc
finrange
	sta filter_dshim1s,x
	tya
	sec
	sbc #$10
	tay
	inx
	cpx #$10
	bne filter_tbl_helper_loop
	
	rts

	;loads the next pattern
loadpattern
	ldy #$00
	lda (pattern_index_cursor),y
	sta pattern_data_cursor
	iny
	lda (pattern_index_cursor),y
	sta pattern_data_cursor+1
	
	lda pattern_index_cursor
	clc
	adc #$02
	sta pattern_index_cursor
	bcc loadpattern0
	inc pattern_index_cursor+1
loadpattern0
	lda pattern_index_cursor
	cmp max_pattern_addr
	bne loadpatternend
	lda pattern_index_cursor+1
	cmp max_pattern_addr+1
	bne loadpatternend
	
	//TODO: Support no-loop ($ff)

loop_addr_lo
	lda #$00
	sta pattern_index_cursor
loop_addr_hi
	lda #$00
	sta pattern_index_cursor+1
	
loadpatternend
	rts
	
	;executes the next frame in the HARDBASS tracker tune
	;and sets the parameters for the HARDBASS sound engine
dotracker
	.ifdef debugtracker
	inc trackerframe	
	.endif

	lda pattern_skip_frames
	cmp #$01
	beq dotracker_process_1st_pattern_command
	dec pattern_skip_frames
	jmp dotracker_after_cmd_proc

dotracker_process_1st_pattern_command
	ldy #$00
dotracker_process_next_pattern_command
	lda (pattern_data_cursor),y
	cmp #$80
	beq dotracker_endpattern
	bpl dotracker_hasskipfr
	tax
	lda #$00
	sta dotracker_process_command_skipfr+1
	lda pattern_commands_lo,x
	sta dotracker_cmd_jump2+1
	lda pattern_commands_hi,x
	sta dotracker_cmd_jump2+2
dotracker_cmd_jump2
	jmp $1111
	
dotracker_endpattern
	jsr loadpattern
	jmp dotracker_process_1st_pattern_command
	
dotracker_hasskipfr	
	tax
	and #$78
	lsr	
	lsr	
	lsr
	sta dotracker_process_command_skipfr+1
	txa
	and #$07
	tax
	lda pattern_commands_lo,x
	sta dotracker_cmd_jump+1
	lda pattern_commands_hi,x
	sta dotracker_cmd_jump+2
dotracker_cmd_jump
	jmp $1111	
	
dotracker_cmd_0 ;EXPORT_COMMAND_SKIP_FRAMES
	iny
	lda (pattern_data_cursor),y
	sta dotracker_process_command_skipfr+1

	jmp dotracker_aftercmd	
	
dotracker_cmd_1 ;EXPORT_COMMAND_NOTE_CHANGE
	iny
	lda (pattern_data_cursor),y
	bmi set_note_vib
	
	tax
	lda note_tbl_lo_addr,x
	sta baseinclo+1
note_tbl_hi_addr
	lda $1111,x
	sta baseinchi+1

	lda instrFlags
	and #$40
	beq dotracker_after_set_note_vib
	
set_note_vib
	iny
	lda (pattern_data_cursor),y
	sta vib_depth+1
	iny
	lda (pattern_data_cursor),y
	sta vib_inc_lo+1
	lda #$00
	sta vib_inc_hi+1

dotracker_after_set_note_vib
	jmp dotracker_aftercmd	
	
dotracker_cmd_2 ;EXPORT_COMMAND_FLAGS_CHANGE
	ldx instrFlags
	iny
	lda (pattern_data_cursor),y
	sta instrFlags
	bmi turnSoundOn
	
turnSoundOff
	txa
	bpl sound_already_off
	
	lda #$00
	sta dsincl+1
	sta dsinch+1
	
sound_already_off
	jmp dotracker_aftercmd

turnSoundOn
	txa
	bmi sound_already_on

	lda #$00
	sta pwlfo_value+1
	sta dotracker_pwlfo_reverse_on_off+1
	sta cutofflfo_value+1
	sta dotracker_cutofflfo_reverse_on_off+1
	sta vib_value_lo+1
	sta vib_value_hi+1
	sta dotracker_vib_lo_reverse_on_off+1
	sta dotracker_vib_hi_reverse_on_off+1

sound_already_on
	jmp dotracker_aftercmd	
		
dotracker_cmd_3; EXPORT_COMMAND_INSTRUMENT_CHANGE
	lda #$00
	sta instrFlags
	sta pwlfo_value+1
	sta dotracker_pwlfo_reverse_on_off+1
	sta cutofflfo_value+1
	sta dotracker_cutofflfo_reverse_on_off+1
	sta vib_value_lo+1
	sta vib_value_hi+1
	sta dotracker_vib_lo_reverse_on_off+1
	sta dotracker_vib_hi_reverse_on_off+1
	
	iny
	lda (pattern_data_cursor),y
	sta pwlfo_depth+1
	iny
	lda (pattern_data_cursor),y
	sta pwlfo_inc+1
	iny
	lda (pattern_data_cursor),y
	sta cutofflfo_depth+1
	iny
	lda (pattern_data_cursor),y
	sta cutofflfo_inc+1

	jmp dotracker_aftercmd	
	
dotracker_cmd_4; EXPORT_COMMAND_CUTOFF_CHANGE
	iny
	lda (pattern_data_cursor),y
	sta basecutoff+1

	jmp dotracker_aftercmd	
	
dotracker_cmd_5; EXPORT_COMMAND_PW_CHANGE
	iny
	lda (pattern_data_cursor),y
	sta basepw+1

	jmp dotracker_aftercmd	
	
dotracker_cmd_6; EXPORT_COMMAND_PORTAMENTO_STAGE_START_FROM_FREQ
	lda #$01
	sta dotracker_porta_on_off+1
	iny
	lda (pattern_data_cursor),y
	sta baseinclo+1
	iny
	lda (pattern_data_cursor),y
	sta baseinchi+1
	iny
	lda (pattern_data_cursor),y
	sta portaddinclo+1
	iny
	lda (pattern_data_cursor),y
	sta portaddinchi+1
	
	jmp dotracker_aftercmd	
	
dotracker_cmd_7; EXPORT_COMMAND_PORTAMENTO_END
	lda #$80
	sta dotracker_porta_on_off+1

	jmp dotracker_aftercmd	
	
dotracker_aftercmd
	iny
	
dotracker_process_command_skipfr
	lda #$00
	bne dotracker_store_skipfr
	jmp dotracker_process_next_pattern_command
dotracker_store_skipfr	
	sta pattern_skip_frames

	tya
	ldy #$00
	clc
	adc pattern_data_cursor
	sta pattern_data_cursor
	bcc dotracker_after_cmd_proc
	inc pattern_data_cursor+1

dotracker_after_cmd_proc

dotracker_porta_on_off
	lda #$00
	beq dotracker_process_porta
	bmi dotracker_no_porta
	dec dotracker_porta_on_off+1
	jmp dotracker_no_porta

dotracker_process_porta

portaddinclo
	lda #$00
	clc
	adc baseinclo+1
	sta baseinclo+1
	
portaddinchi
	lda #$00
	adc baseinchi+1
	sta baseinchi+1

dotracker_no_porta	

	//process LFOs / Vibrato

	lda instrFlags
	and #$10
	beq dotracker_nopwlfo

	//process PW LFO
pwlfo_value
	lda #$00
	clc
pwlfo_inc
	adc #$00
	sta pwlfo_value+1
	bne pwlfo_check_range
	lda dotracker_pwlfo_reverse_on_off+1
	eor #$ff
	sta dotracker_pwlfo_reverse_on_off+1	
	jmp dotracker_pwlfo_reverse

pwlfo_check_range
	
pwlfo_depth
	cmp #$00
	bcc dotracker_pwlfo_within_range

dotracker_pwlfo_reverse	
	lda pwlfo_inc+1
	eor #$ff
	adc #$00 ;C is set already
	sta pwlfo_inc+1
	
dotracker_pwlfo_within_range

dotracker_nopwlfo



	lda instrFlags
	and #$20
	beq dotracker_nocutofflfo

	//process cutoff LFO
cutofflfo_value
	lda #$00
	clc
cutofflfo_inc
	adc #$00
	sta cutofflfo_value+1
	bne cutofflfo_check_range
	lda dotracker_cutofflfo_reverse_on_off+1
	eor #$ff
	sta dotracker_cutofflfo_reverse_on_off+1	
	jmp dotracker_cutofflfo_reverse

cutofflfo_check_range
	
cutofflfo_depth
	cmp #$00
	bcc dotracker_cutofflfo_within_range

dotracker_cutofflfo_reverse	
	lda cutofflfo_inc+1
	eor #$ff
	adc #$00 ;C is set already
	sta cutofflfo_inc+1
	
dotracker_cutofflfo_within_range

dotracker_nocutofflfo




	lda instrFlags
	bmi dotracker_sound_is_on
	jmp dotracker_nosound	

dotracker_sound_is_on

	and #$40
	beq dotracker_novib

	//process vibrato

vib_value_lo
	lda #$00
	clc
vib_inc_lo
	adc #$00
	sta vib_value_lo+1
vib_value_hi
	lda #$00
vib_inc_hi
	adc #$00
	sta vib_value_hi+1
	cmp #$ff
	bne vib_check_range
	lda dotracker_vib_lo_reverse_on_off+1
	eor #$ff
	sta dotracker_vib_lo_reverse_on_off+1	
	lda dotracker_vib_hi_reverse_on_off+1
	eor #$ff
	sta dotracker_vib_hi_reverse_on_off+1	
	jmp dotracker_vib_reverse

vib_check_range
	lda vib_value_hi+1
	cmp #$00
	bcc dotracker_vib_within_range
	bne dotracker_vib_reverse
	lda vib_value_lo+1
vib_depth
	cmp #$00
	bcc dotracker_vib_within_range
	
dotracker_vib_reverse	
	lda vib_inc_lo+1
	eor #$ff
	adc #$00 ;C is set already
	sta vib_inc_lo+1
	lda vib_inc_hi+1
	eor #$ff
	sta vib_inc_hi+1
	
dotracker_vib_within_range

dotracker_novib

	//update sound registers

	lda vib_value_lo+1
dotracker_vib_lo_reverse_on_off
	eor #$00
	clc
baseinclo
	adc #$00
	sta dsincl+1

	lda vib_value_hi+1
dotracker_vib_hi_reverse_on_off
	eor #$00
baseinchi
	adc #$00
	sta dsinch+1

	
	lda pwlfo_value+1
dotracker_pwlfo_reverse_on_off
	eor #$00
	clc
basepw
	adc #$00
	sta dspw+1


	lda cutofflfo_value+1
dotracker_cutofflfo_reverse_on_off
	eor #$00
	clc
basecutoff
	adc #$00
	tax
	lda filter_tbls,x
	sta dsflttbl+1
	lda filter_dshim1s,x
	sta dshim1+1
	
dotracker_nosound
	
	rts

;RMT will call this when needed
HARDBASS_SOUND_ENABLE
	nop
	lda #$60
	sta HARDBASS_SOUND_ENABLE ;rts
	lda #$ea
	sta HARDBASS_SOUND_DISABLE ;nop

	lda #$8c ;sty abs
	sta styaudc
	lda #$8e ;stx abs
	sta stxaudf
	lda #$8d ;sta abs
	sta staaudf
	lda #timer
	sta timerena
	rts

;RMT will call this when needed
HARDBASS_SOUND_DISABLE
	nop
	lda #$60
	sta HARDBASS_SOUND_DISABLE ;rts
	lda #$ea
	sta HARDBASS_SOUND_ENABLE ;nop

	lda #$00
	sta IRQEN
	
	lda #$ac ;ldy abs
	sta styaudc
	sta stxaudf ;we store ldy here too, ldx wouldn't work
	lda #$ad ;lda abs
	sta staaudf
	lda #$00
	sta timerena
	rts

	;copies our zero page code to the zero page
	;todo: remove this copy stuff and load straight into the zero page
copyzp	ldy #0
zpcopy 	mva .adr(zpcode),y zpcode,y+
	cpy #.len zpcode
	bne zpcopy
	rts

	;zero page code
	;this gives us some speed increase for our IRQs, but not huge
	;so feel free to do this somewhere else in the memory if
	;you need to squeeze out the last bytes from your zero page
	.local zpcode, zpvarsend
	
irq	sta isa+1
	lda #0
	sta irqen

irqsplcnt
sb2	lda samplebuf+$18 ;this is page start as the buffer is aligned to xxe8
	sta sample_register

	.ifndef debugtimers
	.ifndef testcolors
	.ifdef visualization
	sta COLBK
	.endif
	.endif
	.endif

	.ifdef debugtimers
	and #$e0
	beq debugok
	nop ;wrong value - place breakpoint here to notice
debugok
	.endif

	inc irqsplcnt+1

irqtpt
	lda timerpattern+5
	sta AUDF1+(timer-1)*2
	dec irqtpt+1
	bpl irqmorept
	lda #timerpatternmax
	sta irqtpt+1
	
irqmorept
	
	lda #timer
	sta irqen

isa	lda #0
	rti
	
acpycnt .ds 0

	.endl

max_pattern_addr
	.ds 2
	
pattern_skip_frames
	.ds 1
	
	.ifdef debugtracker
trackerframe
	.ds 1
	.endif

pattern_commands_lo
	.byte <dotracker_cmd_0,<dotracker_cmd_1,<dotracker_cmd_2,<dotracker_cmd_3
	.byte <dotracker_cmd_4,<dotracker_cmd_5,<dotracker_cmd_6,<dotracker_cmd_7

pattern_commands_hi
	.byte >dotracker_cmd_0,>dotracker_cmd_1,>dotracker_cmd_2,>dotracker_cmd_3
	.byte >dotracker_cmd_4,>dotracker_cmd_5,>dotracker_cmd_6,>dotracker_cmd_7

hbassdata	
	INS "export.hardbass"

	song_loop_pos_addr = hbassdata+8
	note_count_addr = hbassdata+9
	note_tbl_lo_addr = hbassdata+10

	//..

	.if *>=(PLAYER-$320)
	;address check based on advise from TeBe
	ert "Code before RMT Player got too long! Address: "*
	.else
	.print "Space left before RMT Player: ",PLAYER-$320-*," (address: ",*,")"
	.endif

	org PLAYER-$320
rmt_player_work_area
	.ds $320

	.def STEREOMODE=0
	.def HARDBASS=1
	ICL "rmtplayr.asm"
	
rmt_player_end

	org $d800
	opt h- ;RMT module is standard Atari binary file already
	ins "reharden.rmt" ;include music RMT module
	opt h+
rmt_music_data_end

	run start	;Define run address
	
	org $00
pattern_index_cursor
	.ds 2
pattern_data_cursor
	.ds 2

zp_tmp	.ds 2
timerena
	.byte timer

instrFlags .ds 1

zpvarsend