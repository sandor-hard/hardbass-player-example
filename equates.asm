;	'400/800/5200 Equate Tables'
;	By J. Leiterman
;	8/29/82

;	Last Modification 
;	June 2004 C. G. Hutt
;
;*******************************************************
;*** (C) 1982 Atari Inc.  All Rights Reserved.  No   ***
;*** part of this program of publication may be      ***
;*** Reproduced, Transmitted, Transcribed, stored    ***
;*** in a Retrieval System or Translated into any    ***
;*** Language or Computer Language, in any form or   ***
;*** by any means, Electronic, Mechanical, Magnetic, ***
;*** Optical, Chemical, Manual or otherwise, without ***
;*** the prior written permission of Atari Inc. 1196 ***
;*** Borregas Ave.  Sunnyvale  CA 94086              ***
;*******************************************************
;
;	[ATARI]
;	0=400/800	(Candy/Colleen)
;	1=CX5200	(Pam)           


;	.IF	ATARI=0

; 400/800 Candy/Colleen Equates

GTIA	=	$D000	;Gtia Base
POKEY	=	$D200	;Pokey Base
PIA	=	$D300	;Port Base
ANTIC	=	$D400	;Antic Base
CHRORG	=	$E000	;Character Generator Base

CASINI	=	$0002	;Vec. for cassette program init.
BOOT?	=	$0009	;Boot success disk/cassette
DOSVEC	=	$000A	;Software start vector
DOSINI	=	$000C	;Software Init address
POKMSK	=	$0010	;Mask for Pokey IRQ enable
BRKKEY	=	$0011	;BREAK flag
RTCLOK	=	$0012	;60 hz. clock
CRITIC	=	$0042	;Critical section
ATRACT	=	$004D	;Attract Mode
COLDST	=	$0244	;Coldstart Flag
CARTCK	=	$03EB	;Cartridge Checksum
GINTLK	=	$03FA	;Cartridge Interlock Register


;Interrupt Vectors
                 
VDSLST	=	$0200	;Display List Interrupt NMI 
VPRCED	=	$0202	;Proceed Line IRQ 
VINTER	=	$0204	;Interrupt Line IRQ
VBREAK	=	$0206	;Software Break IRQ
VKEYBD	=	$0208	;Keyboard IRQ
VSERIN	=	$020A	;Serial Input Rdy. IRQ
VSEROR	=	$020C	;Serial Output Rdy. IRQ
VSEROC	=	$020E	;Serial Output Complete IRQ
VTIMR1	=	$0210	;Pokey Timer 1 IRQ
VTIMR2	=	$0212	;Pokey Timer 2 IRQ
VTIMR4	=	$0214	;Pokey Timer 4 IRQ
VIMIRQ	=	$0216	;Vector to IRQ Handler
VVBLKI	=	$0222	;Immediate Vertical Blank NMI
VVBLKD	=	$0224	;Deferred Vertical Blank
CDTMA1	=	$0226	;#1 System Timer JSR Address
CDTMA2	=	$0228	;#2	"
SDMCTL	=	$022F	;DMACTL Shadow
SDLSTL	=	$0230	;DLISTL	"
SDLSTH	=	$0231	;DLISTH 	"
SSKCTL	=	$0232	;Serial Port control
LPENH	=	$0234	;PENH Shadow
LPENV	=	$0235	;PENV Shadow
BRKKY	=	$0236	;BREAK Key Vector (Only Rev. B)
GPRIOR	=	$026F	;PRIOR Shadow

PADDL0	=	$0270	;POT0 Shadow
PADDL1	=	$0271	;POT1  "
PADDL2	=	$0272	;POT2  "
PADDL3	=	$0273	;POT3  "
PADDL4	=	$0274	;POT4  "
PADDL5	=	$0275	;POT5  "
PADDL6	=	$0276	;POT6  "
PADDL7	=	$0277	;POT7  "

STICK0	=	$0278	;0 Joystick
STICK1	=	$0279	;1  "
STICK2	=	$027A	;2  "
STICK3	=	$027B	;3  "

STRIG0	=	$0284	;TRIG0 Shadow
STRIG1	=	$0285	;TRIG1  "
STRIG2	=	$0286	;TRIG2  "
STRIG3	=	$0287	;TRIG3  "
                                
SHFLK	=	$02BE	;Shift-Lock flag
           
PCOLR0	=	$02C0	;COLPM0 Shadow
PCOLR1	=	$02C1	;COLPM1  "
PCOLR2	=	$02C2	;COLPM2  "
PCOLR3	=	$02C3	;COLPM3  "

COLOR0	=	$02C4	;COLPF0 Shadow
COLOR1	=	$02C5	;COLPF1  "
COLOR2	=	$02C6	;COLPF2  "
COLOR3	=	$02C7	;COLPF3  "
COLOR4	=	$02C8	;COLBK   "
                                   
MEMLO	=	$02E7	;Start of user memory
CHACT	=	$02F3	;CHACTL Shadow
CHBAS	=	$02F4	;CHBASE  "
CH	=	$02FC	;KBCODE  "

                         
;	Device Control Block

DDEVIC	=	$0300	;Device Bus I.D.
DUNIT	=	$0301	;Device unit number
DCOMND	=	$0302	;Device command
DSTATS	=	$0303	;Device status
DBUFLO	=	$0304	;Lo Handler Buffer Address 
DBUFHI	=	$0305	;Hi
DTIMLO	=	$0306	;Device Timeout
DBYTLO	=	$0308	;Lo Buffer Length
DBYTHI	=	$0309	;Hi
DAUX1	=	$030A	;Aux 1
DAUX2	=	$030B	;Aux 2


;	I/O Control Block

IOCB	=	$0340
ICHID	=	IOCB	;Handler I.D.
ICDNO	=	IOCB+1	;Device number
ICCOM	=	IOCB+2	;Command Byte
ICSTA	=	IOCB+3	;Status
ICBAL	=	IOCB+4	;Lo Buffer Address
ICBAH	=	IOCB+5	;Hi 
ICPTL	=	IOCB+6	;Lo Put Byte Vector
ICPTH	=	IOCB+7	;Hi
ICBLL	=	IOCB+8	;Lo Buffer Length
ICBLH	=	IOCB+9	;Hi
ICAX1	=	IOCB+$A	;Aux. #1
ICAX2	=	IOCB+$B	;Aux. #2
ICSPR	=	IOCB+$C	;Spare bytes for handler use
                  

;	Pia I/O chip

PORTA	=	PIA	;Port A Data
PORTB	=	PIA+1	;Port B Data
PACTL	=	PIA+2	;Port A Control
PBCTL	=	PIA+3	;Port B Control


;	Rom Vectors                           

DISKIV	=	$E450	;Disk handler Init.
DSKINV	=	$E453	;Disk handler
CIOV	=	$E456	;Central I/O 
SIOV	=	$E459	;Serial I/O 
SETVBV	=	$E45C	;Set system timers
SYSVBV	=	$E45F	;System Vertical Blank
XITVBV	=	$E462	;Exit Vertical Blank
SIOINV	=	$E465	;Serial I/O Init.
SENDEV	=	$E468	;Serial bus send enable
INTINV	=	$E46B	;Interrupt handler
CIOINV	=	$E46E	;Central I/O Init.
BLKBDV	=	$E471	;Blackboard mode (Memopad)
WARMSV	=	$E474	;Warm start entry (System Reset)
COLDSV	=	$E477	;Cold start entry (Power-up)
RBLOKV	=	$E47A	;Cassette read block
CSOPIV	=	$E47D	;Cassette Open for Input
    

; Cartridge Parameters

CARTCS	=	$BFFA	;Cartridge Start Address
CART	=	$BFFC	;0=Cart Exists
CARTFG	=	$BFFD	;Option Byte
			;D7  0=Not a Diagnostic Cart
			;    1=Is a Diagnostic cart and control is 
			;      given to cart before any OS is init.
			;D2  0=Init but Do not Start Cart
			;    1=Init and Start Cart
			;D0  0=Do not boot disk
			;    1=Boot Disk
CARTAD	=	$BFFE	;Cartridge Init. Address

;**************************
;***   GTIA Registers   ***
;**************************

CTIA	=	GTIA 
HPOSP0	=	GTIA+$00 ;0 Player Horz. position
HPOSP1	=	GTIA+$01 ;1	"
HPOSP2	=	GTIA+$02 ;2	"
HPOSP3	=	GTIA+$03 ;3	"

HPOSM0	=	GTIA+$04 ;0 Missile Horz. position
HPOSM1	=	GTIA+$05 ;1	"
HPOSM2	=	GTIA+$06 ;2	"
HPOSM3	=	GTIA+$07 ;3	"

SIZEP0	=	GTIA+$08 ;0 Player Size
SIZEP1	=	GTIA+$09 ;1	"
SIZEP2	=	GTIA+$0A ;2	"
SIZEP3	=	GTIA+$0B ;3	"

SIZEM	=	GTIA+$0C ;Missiles size

M0PF	=	GTIA+$00 ;0 Missile to playfield collision
M1PF	=	GTIA+$01 ;1	"
M2PF	=	GTIA+$02 ;2	"
M3PF	=	GTIA+$03 ;3	"

P0PF	=	GTIA+$04 ;0 Player to playfield collision
P1PF	=	GTIA+$05 ;1	"
P2PF	=	GTIA+$06 ;2	"
P3PF	=	GTIA+$07 ;3	"

M0PL	=	GTIA+$08 ;0 Missile to player collision
M1PL	=	GTIA+$09 ;1	"
M2PL	=	GTIA+$0A ;2	"
M3PL	=	GTIA+$0B ;3	"

P0PL	=	GTIA+$0C ;0 Player to Player collision
P1PL	=	GTIA+$0D ;1	"
P2PL	=	GTIA+$0E ;2	"
P3PL	=	GTIA+$0F ;3	"

GRAFP0	=	GTIA+$0D ;0 Player Graphics
GRAFP1	=	GTIA+$0E ;1	"
GRAFP2	=	GTIA+$0F ;2	"
GRAFP3	=	GTIA+$10 ;3	"

GRAFM	=	GTIA+$11 ;Missile Graphics

TRIG0	=	GTIA+$10 ;0 Joystick Trigger
TRIG1	=	GTIA+$11 ;1	" 
TRIG2	=	GTIA+$12 ;2	"
TRIG3	=	GTIA+$13 ;3	"

COLPM0	=	GTIA+$12 ;0 Player/Missile Color
COLPM1	=	GTIA+$13 ;1	"
COLPM2	=	GTIA+$14 ;2	"
COLPM3	=	GTIA+$15 ;3	"

PAL	=	GTIA+$14 ;Determine PAL/NTSC

COLPF0	=	GTIA+$16 ;0 Playfield Color
COLPF1	=	GTIA+$17 ;1	"
COLPF2	=	GTIA+$18 ;2	"
COLPF3	=	GTIA+$19 ;3	"
COLBK	=	GTIA+$1A ;Background/Border Color

PRIOR	=	GTIA+$1B ;Display Depth Priorities
VDELAY	=	GTIA+$1C ;Vertical Delay
GRACTL	=	GTIA+$1D ;Graphics Control
HITCLR	=	GTIA+$1E ;Clear All P/M collision Reg.
CONSOL	=	GTIA+$1F ;Console Key register


;**************************
;***   POKEY Registers  ***
;**************************

AUDF1	=	POKEY+$00 ;1 Audio Frequency
AUDF2	=	POKEY+$02 ;2	"
AUDF3	=	POKEY+$04 ;3	"
AUDF4	=	POKEY+$06 ;4	"

AUDC1	=	POKEY+$01 ;1 Audio Control
AUDC2	=	POKEY+$03 ;2	"
AUDC3	=	POKEY+$05 ;3	"
AUDC4	=	POKEY+$07 ;4	"

AUDCTL	=	POKEY+$08 ;Audio Master Control

POT0	=	POKEY+$00 ;0 Analog in port
POT1	=	POKEY+$01 ;1	"
POT2	=	POKEY+$02 ;2	"
POT3	=	POKEY+$03 ;3	"
POT4	=	POKEY+$04 ;4	"
POT5	=	POKEY+$05 ;5	"
POT6	=	POKEY+$06 ;6	"
POT7	=	POKEY+$07 ;7	"

ALLPOT	=	POKEY+$08 ;Read 8 Line Pot Port status

STIMER	=	POKEY+$09 ;Start Timer
KBCODE	=	POKEY+$09 ;Keyboard Code
SKRES	=	POKEY+$0A ;Reset Serial Port Status
RANDOM	=	POKEY+$0A ;Random Number Generator
POTGO	=	POKEY+$0B ;Start Pot Scan s=ence
SEROUT	=	POKEY+$0D ;Serial Port Output
SERIN	=	POKEY+$0D ;Serial Port Input
IRQEN	=	POKEY+$0E ;IRQ enable mask
IRQST	=	POKEY+$0E ;IRQ status
SKCTL	=	POKEY+$0F ;serial port control
SKSTAT	=	POKEY+$0F ;Serial port status


;**************************
;***   ANTIC Registers  ***
;**************************

DMACTL	=	ANTIC+$00 ;DMA Control 
CHACTL	=	ANTIC+$01 ;Character Set Control
DLISTL	=	ANTIC+$02 ;Display List Low
DLISTH	=	ANTIC+$03 ;             High
HSCROL	=	ANTIC+$04 ;Horz. Fine Scroll 
VSCROL	=	ANTIC+$05 ;Vert. Fine Scroll
PMBASE	=	ANTIC+$07 ;Player/Missile Base
CHBASE	=	ANTIC+$09 ;Character Set Base
WSYNC	=	ANTIC+$0A ;Wait for Horz. Sync
VCOUNT	=	ANTIC+$0B ;Scan Line Counter
PENH	=	ANTIC+$0C ;Horz. Light Pen
PENV	=	ANTIC+$0D ;Vert. Light Pen
NMIEN	=	ANTIC+$0E ;Non-maskable Interrupt enable
NMIRES	=	ANTIC+$0F ;NMI reset
NMIST	=	ANTIC+$0F ;MNI status
                                   

;****************************
;*** Display List Equates ***
;****************************

;   Graphic Modes vary from 2-F

BLANK1	=	$00	;1 Blank Scan Line
BLANK2	=	$10	;2 Blank Scan Lines
BLANK3	=	$20	;3 	"
BLANK4	=	$30	;4	"
BLANK5	=	$40	;5 	"
BLANK6	=	$50	;6 	"
BLANK7	=	$60	;7 	"
BLANK8	=	$70	;8	"

RLDMSC	=	$40	;This + Graphic Mode=pointer
				; to memory for scan line
JMPWT	=	$41	;Point to top to Display List
	
HSC	=	$10	;Fine Horz. Scroll Enable
VSC	=	$20	;Fine Vert. Scroll Enable
INT	=	$80	;Display List Interrupt Set


