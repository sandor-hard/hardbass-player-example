# HARDBASS+RMT Player Example

## What does this repo include?

This repo includes the full source code of a HARDBASS+RMT Player example project for the Atari XL/XE.
Use the [WUDSN IDE](https://www.wudsn.com/) to compile and run hardbassexample.asm.

Please click the link below for details on the HARDBASS soft-synth:

- [HARDBASS soft-synth documentation](https://bitbucket.org/sandor-hard/reharden/src/master/HARDBASS.md)
- [HARDBASS Tracker for Android](https://bitbucket.org/sandor-hard/hardbass-tracker-android/)

Sandor Teli / HARD
